import { jobs } from "../../../data";

export default async (req, res) => {
  const { method } = req;

  switch (method) {
    case "GET":
      returnJobById(req, res);
      break;

    case "PUT":
      updateJobById(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT", "DELETE"]);
      res.status(405).end(`Metodo ${method} no permitido`);
  }
};

const returnJobById = (req, res) => {
  const {
    query: { jobId },
  } = req;
  const result = jobs.find((job) => job.id === parseInt(jobId));
  if (result) {
    return res.status(200).json(result);
  }
  return res
    .status(404)
    .json({ message: `Trabajo con id ${jobId} no encontrado` });
};

const updateJobById = (req, res) => {
  const {
    query: { jobId, title, company, location },
  } = req;

  if (!title || !company || !location) {
    return res.status(404).json({
      message: `Los campos title, company y location son obligatorios`,
    });
  }

  let result = jobs.find((job) => job.id === parseInt(jobId));

  if (result) {
    result = { id: result.id, title, company, location };
    return res.status(200).json(result);
  }
  return res
    .status(404)
    .json({ message: `Trabajo con id ${jobId} no encontrado` });
};
